import React, { createContext, useContext, useLayoutEffect, useState } from 'react'
import { LayoutContext, LayoutManagerProps, LayoutProps } from "../index";

const layoutValue: LayoutContext = {
	currentLayout: 'default',
	defaultLayout: 'default',
	setCurrentLayout: (_: string) => {},
	layouts: {},
}

export const layoutContext = createContext<typeof layoutValue>(layoutValue)

const LayoutRenderer = ({ children }: LayoutProps) => {
	const context = useContext(layoutContext)
	const LayoutToRender = context.layouts[context.currentLayout]

	return (
	// @ts-ignore
		<LayoutToRender>
			{ children }
		</LayoutToRender>
	)
}

export const LayoutManager = (props: LayoutManagerProps) => {
	const [ currentLayout, setCurrentLayout ] = useState(props.defaultLayout)

	return (
		<layoutContext.Provider
			value={{
				layouts: props.layouts,
				defaultLayout: props.defaultLayout,
				currentLayout: currentLayout,
				setCurrentLayout,
			}}
		>
			<LayoutRenderer>
				{ props.children }
			</LayoutRenderer>
		</layoutContext.Provider>
	)
}

export const useLayout = (layout: string) => {
	const context = useContext(layoutContext)


	useLayoutEffect(() => {
		if (!context.layouts[layout]) {
			console.error(`Layout '${layout}' does not exist. Fallback to default one`)
			context.setCurrentLayout(context.defaultLayout)
			return
		}
		context.setCurrentLayout(layout)
		return () => context.setCurrentLayout(context.defaultLayout)
	}, [ layout, context ])
}

export default LayoutManager
