## react-layouts-manager-hook
Simple hook-enabled layout system for react 

#### Install
```shell script
    npm install --save react-layouts-manager-hook
```

#### Demo
Working demo is available under this link:

https://codesandbox.io/s/react-layouts-manager-hook-rtql2
