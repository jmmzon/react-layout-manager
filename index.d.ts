import {ReactNode, FC, Context} from 'react'

type Dictionary<T = any> = {
    [key: string]: T
}

export type LayoutProps = { children: ReactNode }

export type LayoutType = (props: { children: ReactNode }) => ReactNode

export type LayoutManagerProps = {
    children: ReactNode,
    layouts: Dictionary<LayoutType>,
    defaultLayout: string,
}

export type LayoutContext = {
    currentLayout: string,
    defaultLayout: string,
    setCurrentLayout: Function,
    layouts: Dictionary<LayoutType>
}

declare const LayoutManager: FC<LayoutManagerProps>

export const useLayout: (layout: string) => any

export const layoutContext: Context<LayoutContext>

declare module 'react-layouts-manager-hooks' {

}

export default LayoutManager
